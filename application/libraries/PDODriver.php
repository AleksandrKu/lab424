<?php

namespace  application\libraries;
class PDODriver
{

	private $connection;
	public function __construct($server, $username, $password, $database)
	{
		$this->connection = new \PDO("mysql:host={$server};dbname={$database}", $username, $password);
	}

	public function getConnection()
	{
		return $this->connection;
	}

	public function __destruct()
	{
		//сам закрывает конекшены
	}

	public function find($table, $id = null)
	{
		if(!empty($id)) {
			$sql = "SELECT * FROM {$table} WHERE id = :id";
			//$sql = "SELECT * FROM {$table} WHERE id = 5";
			$stm = $this->connection->prepare($sql);
			$parametrs = ['id' => $id];
			$execute_res = $stm->execute($parametrs);
			if($execute_res == false) {
				throw new \Exception($stm->errorInfo()[2]);
			}
			return $stm->fetchObject();
		} else {
			$sql = "SELECT * FROM {$table}";
			$stm = $this->connection->query($sql);
			if($stm == false) {
				throw new \Exception($this->connection->errorInfo()[2]);
			}
			return $stm ->fetchAll(\PDO::FETCH_OBJ);
		}

	}



}