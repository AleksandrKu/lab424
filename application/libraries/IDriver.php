<?php
namespace application\libraries;
interface IDriver
{
public function  __construct($server,$username,$password, $database);
public function getConnection();
public function  __destruct();
//public function  find($table, $id = NULL);

}